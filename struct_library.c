#include<stdio.h>
struct bookdetails
{
    char author[20];
    int edition;
};

struct book
{
    char bookname[20];
    int bookid;
    int noc;
    struct bookdetails bd ;
}b;

int main()
{
    printf("\nEnter the details of book 1.");

    printf("\nEnter book name: ");
    scanf("%s",b.bookname);

    printf("\nBookId: ");
    scanf("%d",&b.bookid);

    printf("\nNumber of copies: ");
    scanf("%d",&b.noc);

    printf("\nEnter Author: ");
    scanf("%s",b.bd.author);
 
    printf("\nEnter Edition: ");
    scanf("%d",&b.bd.edition);

    printf("\nThe details of book are:");
    printf("\nName: %s \nBookId: %d \nNo. of Copies: %d \nAuthor: %s \nEdition: %d: ",b.bookname,b.bookid,b.noc,b.bd.author,b.bd.edition);
 
return 0;
}