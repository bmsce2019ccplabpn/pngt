#include <stdio.h>

int main()
{
    int n; int sum=0; float avg;
    printf("\n Enter the number upto which sum has to be calculated : ");
    scanf("%d", &n);
    
    for(int i=0; i<=n; i++)
    {
        sum+=i;
    }
    
    avg=(float)sum/n;
    
    printf("\n The sum of first %d numbers is = %d", n, sum);
    printf("\n The avg of first %d numbers is = %.2f \n", n, avg);    
    return 0;
}