#include<stdio.h>

int input()
{
    int x;
    printf("\n Enter a number : ");
    scanf("%d", &x);
    return x;
}

int total(int x, int y, int z)
{
   return x+y+z;
}

float avg(int x)
{
    float res=(float) x/3;
    return res;
}
    
int smallest(int x, int y, int z)
{
    if(x<y&&x<z)
        return x;
    else if(y<x&&y<z)
        return y;
    else if(z<x&&z<y)
        return z;
}

int largest(int x, int y, int z)
{
    if(x>y&&x>z)
        return x;
    else if(y>x&&y>z)
        return y;
    else if(z>x&&z>y)
        return z;
}

void output(int sum, float avg, int smallest, int largest)
{
    printf("\n The total is %d\n", sum);
    printf("\n The average is %.2f\n", avg);
    printf("\n The smallest of three is %d\n", smallest);
    printf("\n the largest of three is %d\n", largest);
}

int main()
{
    int x=input();
    int y=input();
    int z=input();
    
    int sum=total(x,y,z);
    float average=avg(sum);
    int small=smallest(x,y,z);
    int large=largest(x,y,z);
    
    output(sum, average, small, large);
    
    return 0;
}