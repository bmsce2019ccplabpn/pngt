#include<stdio.h>
int main()
{
    int a[3][3], b[3][3], diff[3][3], i, j;
    
    printf("\n Enter the elements of 3x3 matrix 1:\n");
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            scanf("%d", &a[i][j]);
        }
    }
    
    printf("\n Enter the elements of 3x3 matrix 2:\n");
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            scanf("%d", &b[i][j]);
        }
    }
    
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {   
            diff[i][j]=a[i][j] - b[i][j];
        }
    }
    
    printf("\n The difference of the two matrix is: \n");
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            printf("%d\t",diff[i][j]);
        }
        printf("\n");
    }
    
    return 0;
}