#include<stdio.h>
#include<conio.h>
int strlength(char str[])
{
   int len = 0;
   while (str[len] != '\0')
      len++;
   return (len);
}
 
int main()
{
   char str[100];
   int length;
 
   printf("\n Enter the String : ");
   gets(str);
 
   length = strlength(str);
 
   printf("\n Length of the String is : %d", length);
   getch();
   return 0;
}