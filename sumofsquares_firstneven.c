#include<stdio.h>
#include<math.h>
int main()
{
    int n; int sum=0;
    printf("\n Enter the value of n : ");
    scanf("%d",&n);

    for(int i=1; i<=n ; i++)
    {
        sum=sum+(pow(2*i,2));
    }
    
    printf("The sum of squares of first %d even numbers is = %d\n", n, sum);

    return 0;
}
