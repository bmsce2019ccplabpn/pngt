#include <stdio.h>

int main()
{
    int num, sum=0;
    printf("\n Enter a number: ");
    scanf("%d",&num);
    int temp=num;
    
    while(temp!=0)
    {
        sum+=temp%10;
        temp/=10;
    }

    printf("\n The sum of the digits of %d is= %d \n\n", num, sum);
    return 0;
}