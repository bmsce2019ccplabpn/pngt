#include <stdio.h>

int input()
{
    int x;
    printf("\n Enter a number: ");
    scanf("%d",&x);
    return x;
}

int add(int x, int y)
{
    return x+y;
}

int sub(int x, int y)
{
    return x-y;
}

int prod(int x, int y)
{
    return x*y;
}

int div(int x, int y)
{
    return x/y;
}

int rem(int x, int y)
{
    return x%y;
}

void output(int x)
{
    printf(" The result is: %d", x);
}
int main()
{
    int a=input();
    int b=input();
    int sum=add(a,b);
    int diff=sub(a,b);
    int product=prod(a,b);
    int quotient=div(a,b);
    int remainder=rem(a,b);
    
    printf("\n SUM :");
    output(sum);
    
    printf("\n DIFFERENCE :");
    output(diff);
    
    printf("\n PRODUCT :");
    output(product);
    
    printf("\n QUOTIENT :");
    output(quotient);
    
    printf("\n REMAINDER :");
    output(remainder);
    
    return 0;
}
    