/* Write a C program to find the distance between 2 points on a two-dimensional
plane using functions(user defined) and structure for storing points. */

#include<stdio.h>
#include<math.h>
struct point
{
    float x;
    float y;
};
typedef struct point POINT;

POINT input();
float distance(POINT p1, POINT p2);
void output(POINT p1, POINT p2, float d);

int main()
{
    POINT p1,p2;
    p1=input(); p2=input();
    float d=distance(p1, p2);
    output(p1, p2, d);
    return 0;
}

POINT input()
{
    POINT temp;
    printf("\nEnter the (x,y) co-ordinates: ");
    scanf("%f %f",&temp.x,&temp.y);
    return temp;
}

float distance(POINT p1, POINT p2)
{
    float d=(float)sqrt(pow((p2.y-p1.y),2)+pow((p2.x-p1.x),2));
    return d;
}

void output(POINT p1, POINT p2, float d)
{
    printf("\nThe distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f units.", p1.x, p1.y, p2.x, p2.y, d);
}