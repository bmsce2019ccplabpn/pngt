#include<stdio.h>
int main()
{
    int n,i, smallest, largest, pl, ps, choice;
    
    printf("\n Enter how many elements you want to enter : ");
    scanf("%d",&n);
    
    int arr[n];
    for(i=0; i<n; i++)
    {
        printf("\n Enter element %d : ",i+1);
        scanf("%d", &arr[i]);
    }
    
    smallest=arr[0]; largest=arr[0];
    for(i=1; i<n; i++)
    {
        if(arr[i]<smallest)
            smallest=arr[i];
        if(arr[i]>largest)
            largest=arr[i];
    }
    
    for(i=0; i<n; i++)
    {
        if(arr[i]==smallest)
            ps=i+1;
        if(arr[i]==largest)
            pl=i+1;
    }
    
    printf("\n The Largest element is: %d at postion: %d", largest, pl);
    printf("\n The Smallest element is: %d at position: %d\n", smallest, ps);
    
    
    printf("\nYou want to exchange Largest-Smallest places? (1-Yes, 2-No): ");
    scanf("%d",&choice);
    if(choice==1)
    {
        printf("\n\n-----Interchanging-----\n");
        
        int temp=arr[ps-1];
        arr[ps-1]=arr[pl-1];
        arr[pl-1]=temp;
        
        int tem=pl;
        pl=ps;
        ps=tem;
    }

    printf("\n The positions of largest and smallest after interchanging are : \n Largest at: %d\t Smallest at: %d", pl, ps);
    
    printf("\n The elements of array are:\t");
    for(int j=0; j<n; j++)
    {
        printf("%d\t",arr[j]);
    }
    printf("\n");

    
    return 0;
}