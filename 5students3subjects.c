#include<stdio.h>
int main()
{
    int marks[5][3], i, j, sno, h=0;
    
    for(i=0; i<5; i++)
    {
        printf("\n Enter the marks of Student %d in 3 subjects: \n",i+1);
        for(j=0; j<3; j++)
        {
            printf("Subject %d: ",j+1);
            scanf("%d",&marks[i][j]);
        }
    }
    
    for(j=0; j<3; j++)
    {
        for(i=0; i<5; i++)
        {
            if(marks[i][j]>h)
            {
                h=marks[i][j];
                sno=i+1;
            }
        }
        printf("\n The highest marks in subject %d is %d and scored by student %d.",j+1, h, sno);
        h=0;
    }

return 0;
}