#include<stdio.h>
int main()
{
    int n, i, ele, pos=-1;
    printf("\n Enter the size of the the array: ");
    scanf("%d",&n);
    
    int arr[n];
    
    
    for(i=0; i<n; i++)
    {
        printf("\n Enter the element no %d: ", i+1);
        scanf("%d",&arr[i]);
    }
    
    printf("\n\n Enter the element to be searched: ");
    scanf("%d", &ele);
    
    for(i=0; i<n; i++)
    {
        if(arr[i]==ele)
        {   
            pos=i;
            printf("\n The element %d is found at position %d in the array", ele, pos+1);
        }
    }
    
    if(pos==-1)
        printf("\n Element %d is not found in the array.", ele);
        
return 0;
}