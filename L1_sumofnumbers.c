/* Write a function to find sum of n different numbers. ( Use arrays). */

#include<stdio.h>
int size()
{
    int n;
    printf("Enter how numbers you want to add: ");
    scanf("%d",&n);
    return n;
}
float input()
{
    float x;
    printf("Enter a number: ");
    scanf("%f",&x);
    return x;
}

float add(float a[], int n)
{
    float sum=0.0;
    for(int i=0; i<n; i++)
    {
       sum+=a[i];
    }
    return sum;
}

void output(float a[], float sum, int n)
{
    int i;
    for( i=0; i<n-1; i++)
    {
        printf("%.3f + ",a[i]);
    }
    printf("%.3f = %.3f", a[n-1], sum);
}

int main()
{
    int n;  n=size();
    float arr[n];
    
    for(int i=0; i<n; i++)
        arr[i]=input();
    
    float sum=add(arr, n);
    output(arr, sum, n);
    
    return 0;
}