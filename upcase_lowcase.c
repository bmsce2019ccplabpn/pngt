#include<stdio.h>
#include<conio.h>
int main()
{
    char str[100];
    int i;
    
    printf("\n Enter the string: \n");
    gets(str);
    
    for(i=0; str[i]!='\0'; i++)
    {
        if(str[i]>='A' && str[i]<='Z')
            str[i]=str[i]+32;
        
        else if(str[i]>='a' && str[i]<='z')
            str[i]=str[i]-32;
    }
    
    printf("\n The string after changing the case is: \n");
    puts(str);
    getch();
    return 0;
}