#include<stdio.h>
int main()
{
    int num; int rev=0; int rem=0;
    printf("\n Enter a number : ");
    scanf("%d", &num);
    
    int n=num;
    while(n!=0)
    {
        rem=n%10;
        rev=rev*10+rem;
        n=n/10;
    }
    
    if(rev==num)
        printf("\n The num %d is a palindrome.\n",num);
    else
        printf("\n The num %d is not a palindrome.\n",num);
    
    return 0;
}