/* Write a program to add two fractions.*/

#include<stdio.h>

struct fraction
{
    int n;
    int d;
};
typedef struct fraction fr;

fr input()
{
    fr temp;
    printf("Enter numerator: ");
    scanf("%d",&temp.n);
    printf("Enter denominator: ");
    scanf("%d",&temp.d);
    return temp;
}

int hcf(int a, int b)
{
    while(b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return a;
}

fr add(fr f1, fr f2)
{
    fr sum;
    sum.n = (f1.n*f2.d) + (f2.n*f1.d);
    sum.d = (f1.d*f2.d);
    int f = hcf(sum.n, sum.d);
    sum.n/=f;   sum.d/=f;
    return sum;
}

void output(fr f1, fr f2, fr sum)
{
    printf("\n%d/%d + %d/%d = %d/%d", f1.n, f1.d, f2.n, f2.d, sum.n, sum.d);
}

int main()
{
   fr f1,f2,sum;
   f1 = input(); f2 = input();
   sum = add(f1, f2);
   output(f1, f2, sum);
   return 0;
}