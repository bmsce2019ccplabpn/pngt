#include<stdio.h>
struct student
{
    char name[20];
    int rno;
    float marks;
};

int main()
{
    struct student s1;
    struct student s2;
    
    printf("\n Enter the NAME, ROLL NO, MARKS of Student 1 \n");
    scanf("%s %d %f", s1.name, &s1.rno, &s1.marks);
    
    printf("\n Enter the NAME, ROLL NO, MARKS of Student 2 \n");
    scanf("%s %d %f", s2.name, &s2.rno, &s2.marks);
    
    if(s1.marks==s2.marks)
    {
        printf("\n The marks of both the stuents are equal.\n Their details are: \n");
        printf("Name: %s\t Roll no: %d\t Marks: %.2f\n", s1.name, s1.rno, s1.marks);
        printf("Name: %s\t Roll no: %d\t Marks: %.2f\n", s2.name, s2.rno, s2.marks);
    }
    else if(s1.marks<s2.marks)
    {
        printf("\n Marks of student 2 is greater than student 1. Details are: \n");
        printf("Name: %s\t Roll no: %d\t Marks: %.2f\n", s2.name, s2.rno, s2.marks);
    }
    else
    {
        printf("\n Marks of student 1 is greater than student 2. Details are: \n");
        printf("Name: %s\t Roll no: %d\t Marks: %.2f\n", s1.name, s1.rno, s1.marks);
    }
    
    return 0;
}