#include <stdio.h>
 
int main()
{
   int i, low, high, mid, n, ele, flag=0, array[10];
 
   printf("Enter the size of array: ");
   scanf("%d",&n);
 
   for (i = 0; i < n; i++)
      {
          printf("\n Enter the element no %d:",i+1);
          scanf("%d",&array[i]);
      }
   printf("Enter element to be searched: ");
   scanf("%d", &ele);
 
   low = 0;
   high = n - 1;
   mid = (low+high)/2;
 
   while (low <= high) 
   {
      if (array[mid] == ele) 
      {
         printf("%d found at location %d.\n", ele, mid+1);
         flag=1; break;
      }
      else if (array[mid] < ele)
         low = mid;    
      
      else if(array[mid]> ele)
         high = mid;
 
      mid = (low + high)/2;
   }
   if (flag==0)
      printf("%d Not Found in the array.\n", ele);
      
      return 0;
}