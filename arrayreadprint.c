#include<stdio.h>
int main()
{
    int n, i;
    printf("\n Enter how many elements you want to enter : ");
    scanf("%d",&n);

    int arr[n];
    for(i=0; i<n; i++)
    {
        printf("\n Enter element no %d : ", i+1);
        scanf("%d", &arr[i]);
    }

    printf("\n The elements of array are : ");
    for(i=0; i<n; i++)
    {
        printf(" %d\t",arr[i]);
    }
    printf("\n");

    return 0;
}