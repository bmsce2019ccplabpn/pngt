/* Program to prime numbers between intervals. */

#include <stdio.h>
int main() {
    int a, b, i, flag;
    printf("Enter two numbers(intervals): ");
    scanf("%d %d", &a, &b);
    printf("Prime numbers between %d and %d are: ", a, b);

    while (a < b) 
    {
        flag = 1;

        // if low is a non-prime number, flag will be 0
        for (i = 2; i < a; i++) 
        {
            if (a % i == 0) 
            {
                flag = 0;
                break;
            }
            if(a%i!=0)
            {
                flag = 1;
            }
        }

        if (flag == 1 && a!=1)
            printf("%d ", a);
        a++;
    }

    return 0;
}