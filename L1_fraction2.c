/* Write a program to add n fractions.*/

#include<stdio.h>
struct fraction
{
    int n; 
    int d;
};
typedef struct fraction fr;

int size()
{
    int n;
    printf("Enter how fractions you want to add: ");  scanf("%d",&n);
    return n;
}
void input(fr arr[], int n)
{
    for(int i=0; i<n; i++)
    {
        printf("\nFOR FRACTION %d",i+1);
        printf("\nEnter numerator: ");    scanf("%d", &arr[i].n);
        printf("Enter denominator: ");  scanf("%d", &arr[i].d);
    }
}
int hcf(int a, int b)
{
    while(b!=0)
    {
        int temp=a%b;
        a=b;    b=temp;
    }
    return a;
}
fr add(fr arr[], int n)
{
    fr sum; sum.n=0; sum.d=1;
    for(int i=0; i<n; i++)
    {
        sum.n = (sum.n*arr[i].d) + (arr[i].n*sum.d);
        sum.d = (sum.d*arr[i].d);
    }
    int f = hcf(sum.n, sum.d);
    sum.n/=f;   sum.d/=f;
    return sum;
}
void output(fr arr[], fr sum, int n)
{
    int i;
    for(i=0; i<n-1; i++)
        printf("%d/%d + ", arr[i].n, arr[i].d);
    printf("%d/%d = %d/%d", arr[n-1].n, arr[n-1].d, sum.n, sum.d);
}

int main()
{
   int n=size();
   fr arr[n];
   input(arr, n);
   fr sum = add(arr, n);
   output(arr, sum, n);
   return 0;
}