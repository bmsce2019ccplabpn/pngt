/* Write a C program to find distance between 2 points on a two-dimensional plane using functions(user made). */

#include<stdio.h>
#include<math.h>
void input(float *x1, float *y1, float *x2, float *y2);
float distance(float x1, float y1, float x2, float y2);
void output(float x1, float y1, float x2, float y2, float d);
int main()
{
    float x1, x2, y1, y2;
    input(&x1, &y1, &x2, &y2);
    float d=distance(x1, y1, x2, y2);
    output(x1, y1, x2, y2, d);
    return 0;
}

void input(float *x1, float *y1, float *x2, float *y2)
{
    float px, py;
    printf("\nEnter the (x,y) co-ordinates of point 1: ");
    scanf("%f %f",&px, &py);
    *x1=px; *y1=py;
    printf("\nEnter the (x,y) co-ordinates of point 2: ");
    scanf("%f %f",&px, &py);
    *x2=px; *y2=py;
}

float distance(float x1, float y1, float x2, float y2)
{
    float d=sqrt(pow((y2-y1),2)+pow((x2-x1),2));
    return d;
}

void output(float x1, float y1, float x2, float y2, float d)
{
    printf("\nThe distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f units", x1, y1, x2, y2, d);
}