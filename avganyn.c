#include<stdio.h>
int main()
{
    int n, num, sum=0; float avg;
    printf("\n How many numbers you want to enter : ");
    scanf("%d",&n);
    
    for(int i=0; i<n; i++)
    {
        printf("\n Enter a number: ");
        scanf("%d", &num);
        sum+=num;
    }
    
    avg=(float) sum/n;
    
    printf("\n The avg of the numbers is : %.2f\n", avg);
    
    return 0;
}