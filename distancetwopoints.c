#include<stdio.h>
#include<math.h>

int main()

{
    
    int x1,x2,y1,y2;
    int tempx, tempy;
    float distance;
    printf("Enter X and Y coordinates of first point: ");
    scanf("%d %d",&x1,&y1);
    printf("Enter X and Y coordinates of Second point: ");
    scanf("%d %d",&x2,&y2);
    
    tempx=x2-x1;
    tempy=y2-y1;
    distance=sqrt((tempx*tempx)+(tempy*tempy)); 
    printf("Distance: %.2f",distance);
    
return 0;

}
